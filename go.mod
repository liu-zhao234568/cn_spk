module cncpp

go 1.16

require (
	git.cs.nctu.edu.tw/calee/sctp v1.1.0
	gitee.com/liu-zhao234568/amf v1.2.0
	gitee.com/liu-zhao234568/cntest v1.0.0 // indirect
	gitee.com/liu-zhao234568/smf v1.0.0
	github.com/free5gc/aper v1.0.1
	github.com/free5gc/ausf v1.0.0 // indirect
	github.com/free5gc/flowdesc v1.0.0 // indirect
	github.com/free5gc/fsm v1.0.0 // indirect
	github.com/free5gc/milenage v1.0.0 // indirect
	github.com/free5gc/nas v1.0.5 // indirect
	github.com/free5gc/ngap v1.0.2
	github.com/free5gc/openapi v1.0.3
	github.com/free5gc/pcf v1.0.0 // indirect
	github.com/free5gc/pfcp v1.0.0
	github.com/sirupsen/logrus v1.8.1
)
