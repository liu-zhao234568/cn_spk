package main

import (
	"encoding/hex"
	"io"
	"net"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"git.cs.nctu.edu.tw/calee/sctp"
	"gitee.com/liu-zhao234568/amf/context"
	"gitee.com/liu-zhao234568/amf/factory"
	"gitee.com/liu-zhao234568/amf/logger"
	amf_ngap "gitee.com/liu-zhao234568/amf/ngap"
	"gitee.com/liu-zhao234568/amf/util"
	"github.com/sirupsen/logrus"

	"gitee.com/liu-zhao234568/amf/consumer"
	"gitee.com/liu-zhao234568/amf/nas"
	"gitee.com/liu-zhao234568/smf/pfcp"
	pfcp_message "gitee.com/liu-zhao234568/smf/pfcp/message"
	"gitee.com/liu-zhao234568/smf/pfcp/udp"
	"github.com/free5gc/ngap"
	"github.com/free5gc/pfcp/pfcpType"

	ngap_message "gitee.com/liu-zhao234568/amf/ngap/message"

	"github.com/free5gc/aper"

	smf_context "gitee.com/liu-zhao234568/smf/context"
	smf_factory "gitee.com/liu-zhao234568/smf/factory"
	smf_logger "gitee.com/liu-zhao234568/smf/logger"
	"github.com/free5gc/ngap/ngapConvert"
	"github.com/free5gc/ngap/ngapType"
	"github.com/free5gc/openapi/models"
)

// By uuuuu - entry
// NGAP SCTP 相关设置，照搬，没有修改，因为要跳到自己的 Dispatcher，所以搬过来了
// 摘自 free5gc\NFs\amf\ngap\service\service.go
// NGAPHandler 结构体实现在 main() 里：HandleMessage 对应于 NGAP msg 处理， HandleNotification 对应于 SCTP msg 处理
type NGAPHandler struct {
	HandleMessage      func(conn net.Conn, msg []byte)
	HandleNotification func(conn net.Conn, notification sctp.Notification)
}

// 下面 SCTP 相关都是给 listenAndServe 用的，不想用 Run 来起一个 goroutine 然后等在那里
var sctpConfig sctp.SocketConfig = sctp.SocketConfig{
	InitMsg:   sctp.InitMsg{NumOstreams: 3, MaxInstreams: 5, MaxAttempts: 2, MaxInitTimeout: 2},
	RtoInfo:   &sctp.RtoInfo{SrtoAssocID: 0, SrtoInitial: 500, SrtoMax: 1500, StroMin: 100},
	AssocInfo: &sctp.AssocInfo{AsocMaxRxt: 4},
}

const readBufSize uint32 = 8192

// set default read timeout to 2 seconds
var readTimeout syscall.Timeval = syscall.Timeval{Sec: 2, Usec: 0}

var (
	sctpListener *sctp.SCTPListener
	connections  sync.Map
)

// By uuuuu - end

// By uuuuu - entry
// NGAP handler 的映射 map 与 初始化 init()，自己修改的部分
// 对应的编号要看 NGAP 协议 TS38.413 9.4.7
type HandlerId struct {
	NGAPProcedureType int
	NGAPProcedureId   int64
}

var MyHandlerId HandlerId
var HandlerMap map[string]func(*context.AmfRan, *ngapType.NGAPPDU)
var MyHandler func(*context.AmfRan, *ngapType.NGAPPDU)
var GetMyHandler bool

func init() {
	// 对应的编号要看 NGAP 协议 TS38.413 9.4.7
	HandlerMap = map[string]func(*context.AmfRan, *ngapType.NGAPPDU){
		"1,21": HandleNGSetupRequest,
		"1,15": HandleInitialUEMessage,
		"1,46": HandleUplinkNasTransport,
		"2,14": HandleInitialContextSetupResponse,
		"2,29": HandlePDUSessionResourceSetupResponse,
	}
}

// By uuuuu - end

// By uuuuu - entry
// AMF 基础功能，NGAP SCTP 接收与跳转，大多照搬
// 对应于 free5gc\NFs\amf\amf.go，cli啥的都没用，下面有具体照搬位置
func main() {
	// 对应于 free5gc\NFs\amf\amf.go 里 AMF.Initialize(c)
	// factory.InitConfigFactory，根据 yaml 初始化 var AmfConfig Config
	// 具体各项，见 free5gc\NFs\amf\factory\config.go
	factory.InitConfigFactory("./amfcfg.yaml")

	// 这里直接用 logrus 统一设置一次 log level，就设置为 debug，不像 free5gc 设置那么细
	logger.SetLogLevel(logrus.InfoLevel)

	// 对应于 free5gc\NFs\amf\amf.go 里 AMF.Start()
	// 在这里之前，有 http 设置，给了 4 个服务，对应于 AMF 协议

	// free5gc\NFs\amf\context\context.go 里的 type AMFContext struct
	// func init() 进行了空间的 make
	// util.InitAmfContext 使用 config 来初始化 amfcontext
	// 见 free5gc\NFs\amf\util\init_context.go
	self := context.AMF_Self()
	util.InitAmfContext(self)

	// NRF 没啥用，UPF 也不向 NRF 注册，所以去掉了
	// nrf_factory.InitConfigFactory("./nrfcfg.yaml")
	// nrf_context.InitNrfContext()

	// 以下为 SMF - service/init.go - func (smf *SMF) Start() 里的流程照搬
	smf_factory.InitConfigFactory("./smfcfg.yaml")
	smf_context.InitSmfContext(&smf_factory.SmfConfig)
	smf_context.AllocateUPFID()
	smf_context.InitSMFUERouting(&smf_factory.UERoutingConfig)

	// 继续照搬，下面这个 Run 是 pfcp 的 listen and dispatch，每个 UPF 有一个 go routine
	// for + goroutine 持续监听、读入并跳转到相应处理 handler - pfcp.Dispatch
	udp.Run(pfcp.Dispatch)

	for _, upf := range smf_context.SMF_Self().UserPlaneInformation.UPFs {
		if upf.NodeID.NodeIdType == pfcpType.NodeIdTypeFqdn {
			smf_logger.AppLog.Infof("Send PFCP Association Request to UPF[%s](%s)\n", upf.NodeID.NodeIdValue,
				upf.NodeID.ResolveNodeIdToIp().String())
		} else {
			smf_logger.AppLog.Infof("Send PFCP Association Request to UPF[%s]\n", upf.NodeID.ResolveNodeIdToIp().String())
		}
		pfcp_message.SendPfcpAssociationSetupRequest(upf.NodeID)
	}

	time.Sleep(1000 * time.Millisecond)

	// 回到 AMF ，ngap 对应的处理函数的设置 Dispatch、下面 HandleSCTPNotification 沿用 free5gc 的
	ngapHandler := NGAPHandler{
		HandleMessage:      Dispatch,
		HandleNotification: amf_ngap.HandleSCTPNotification,
	}

	// 该跳转到 ngap_service.Run 里，就直接把 Run 搬过来了
	// 摘自 func Run 在 free5gc\NFs\amf\ngap\service\service.go 里
	// NGAP IP 是从 amfcfg.yaml 里读出来的
	// 用 net.ResolveIPAddr 处理 ip
	ips := []net.IPAddr{}
	for _, addr := range self.NgapIpList {
		if netAddr, err := net.ResolveIPAddr("ip", addr); err != nil {
			logger.NgapLog.Errorf("Error resolving NGAP IP '%s': %v\n", addr, err)
		} else {
			logger.NgapLog.Debugf("NGAP IP '%s'  %s\n", addr, netAddr)
			ips = append(ips, *netAddr)
		}
	}

	// 配置 sctp 的 ip 和 port
	sctpAddr := &sctp.SCTPAddr{
		IPAddrs: ips,
		Port:    38412,
	}

	// go listenAndServe(addr, ngapHandler)
	// 原本是 go routine，之后会注册到 NRF，界面 os.signal 之类的 ctrl+c 中止运行
	// 这里不必起线程了吧，sctp 的 listenandserve 函数里面支持多线程，即支持多用户
	listenAndServe(sctpAddr, ngapHandler)
}

// By uuuuu - end

// By uuuuu - entry
// NGAP SCTP 接收与跳转，照搬
// free5gc\NFs\amf\ngap\service\service.go
// 最后会跳到 go handleConnection(newConn, readBufSize, handler)
// 每个 conn 对应一个 ran，维持。整体是在 for 死循环里，因而支持多 ran 接入
func listenAndServe(addr *sctp.SCTPAddr, handler NGAPHandler) {
	if listener, err := sctpConfig.Listen("sctp", addr); err != nil {
		logger.NgapLog.Errorf("Failed to listen: %+v", err)
		return
	} else {
		sctpListener = listener
	}

	logger.NgapLog.Infof("Listen on %s", sctpListener.Addr())

	for {
		newConn, err := sctpListener.AcceptSCTP()
		if err != nil {
			switch err {
			case syscall.EINTR, syscall.EAGAIN:
				logger.NgapLog.Debugf("AcceptSCTP: %+v", err)
			default:
				logger.NgapLog.Errorf("Failed to accept: %+v", err)
			}
			continue
		}

		var info *sctp.SndRcvInfo
		if infoTmp, err := newConn.GetDefaultSentParam(); err != nil {
			logger.NgapLog.Errorf("Get default sent param error: %+v, accept failed", err)
			if err = newConn.Close(); err != nil {
				logger.NgapLog.Errorf("Close error: %+v", err)
			}
			continue
		} else {
			info = infoTmp
			logger.NgapLog.Debugf("Get default sent param[value: %+v]", info)
		}

		info.PPID = ngap.PPID
		if err := newConn.SetDefaultSentParam(info); err != nil {
			logger.NgapLog.Errorf("Set default sent param error: %+v, accept failed", err)
			if err = newConn.Close(); err != nil {
				logger.NgapLog.Errorf("Close error: %+v", err)
			}
			continue
		} else {
			logger.NgapLog.Debugf("Set default sent param[value: %+v]", info)
		}

		events := sctp.SCTP_EVENT_DATA_IO | sctp.SCTP_EVENT_SHUTDOWN | sctp.SCTP_EVENT_ASSOCIATION
		if err := newConn.SubscribeEvents(events); err != nil {
			logger.NgapLog.Errorf("Failed to accept: %+v", err)
			if err = newConn.Close(); err != nil {
				logger.NgapLog.Errorf("Close error: %+v", err)
			}
			continue
		} else {
			logger.NgapLog.Debugln("Subscribe SCTP event[DATA_IO, SHUTDOWN_EVENT, ASSOCIATION_CHANGE]")
		}

		if err := newConn.SetReadBuffer(int(readBufSize)); err != nil {
			logger.NgapLog.Errorf("Set read buffer error: %+v, accept failed", err)
			if err = newConn.Close(); err != nil {
				logger.NgapLog.Errorf("Close error: %+v", err)
			}
			continue
		} else {
			logger.NgapLog.Debugf("Set read buffer to %d bytes", readBufSize)
		}

		if err := newConn.SetReadTimeout(readTimeout); err != nil {
			logger.NgapLog.Errorf("Set read timeout error: %+v, accept failed", err)
			if err = newConn.Close(); err != nil {
				logger.NgapLog.Errorf("Close error: %+v", err)
			}
			continue
		} else {
			logger.NgapLog.Debugf("Set read timeout: %+v", readTimeout)
		}

		logger.NgapLog.Infof("[AMF] SCTP Accept from: %s", newConn.RemoteAddr().String())
		connections.Store(newConn, newConn)
		// 这里存储 conn，每个 conn 对应一个 ran，维持

		// 注意，整体是在 for 死循环里，因而支持多用户（ran）接入
		go handleConnection(newConn, readBufSize, handler)
	}
}

// By uuuuu - end

// By uuuuu - entry
// NGAP SCTP 这层 处理 SCTP，HandleSCTPNotification 直接沿用 free5gc 的
// 之后跳到 NGAP Dispatch 函数，照搬
func handleConnection(conn *sctp.SCTPConn, bufsize uint32, handler NGAPHandler) {
	defer func() {
		// if AMF call Stop(), then conn.Close() will return EBADF because conn has been closed inside Stop()
		if err := conn.Close(); err != nil && err != syscall.EBADF {
			logger.NgapLog.Errorf("close connection error: %+v", err)
		}
		connections.Delete(conn)
	}()

	// 用于测试 一个 ran 对应 一个 go routine
	// //fmt.Println("\n\none new ran\n\n")

	for {
		buf := make([]byte, bufsize)

		n, info, notification, err := conn.SCTPRead(buf)
		if err != nil {
			switch err {
			case io.EOF, io.ErrUnexpectedEOF:
				logger.NgapLog.Debugln("Read EOF from client")
				return
			case syscall.EAGAIN:
				//logger.NgapLog.Debugln("SCTP read timeout")
				continue
			case syscall.EINTR:
				logger.NgapLog.Debugf("SCTPRead: %+v", err)
				continue
			default:
				logger.NgapLog.Errorf("Handle connection[addr: %+v] error: %+v", conn.RemoteAddr(), err)
				return
			}
		}

		if notification != nil {
			if handler.HandleNotification != nil {
				handler.HandleNotification(conn, notification)
			} else {
				logger.NgapLog.Warnf("Received sctp notification[type 0x%x] but not handled", notification.Type())
			}
		} else {
			if info == nil || info.PPID != ngap.PPID {
				logger.NgapLog.Warnln("Received SCTP PPID != 60, discard this packet")
				continue
			}

			logger.NgapLog.Tracef("Read %d bytes", n)
			logger.NgapLog.Tracef("Packet content:\n%+v", hex.Dump(buf[:n]))

			// TODO: concurrent on per-UE message
			// 因为对于每个 conn 即 每个 ran 进行 for 循环读，UE 相关都在 对应的存储（ue 变量）里，
			// 因而可以并发处理每个 UE 的信息
			// 进入 NGAP dispatch
			handler.HandleMessage(conn, buf[:n])
		}
	}
}

// By uuuuu - end

// By uuuuu - entry
// NGAP SCTP 这层 处理 NGAP，Dispatch 函数，参数为 ran、pdu，有修改
func Dispatch(conn net.Conn, msg []byte) {
	var ran *context.AmfRan
	amfSelf := context.AMF_Self()

	// 在 context 里，ran 与 conn 绑定
	ran, ok := amfSelf.AmfRanFindByConn(conn)
	if !ok {
		logger.NgapLog.Infof("Create a new NG connection for: %s", conn.RemoteAddr().String())
		// 在 context 的 AMF_Ran_pool 里加一个 ran ，ran 只绑定对应的 conn，其他成员 handler 里添加
		ran = amfSelf.NewAmfRan(conn)
	}

	if len(msg) == 0 {
		ran.Log.Infof("RAN close the connection.")
		ran.Remove()
		return
	}

	pdu, err := ngap.Decoder(msg)
	if err != nil {
		ran.Log.Errorf("NGAP decode error : %+v", err)
		return
	}

	// By uuuuu - entry - 2
	// 此处修改，改成 map 映射，使用 reflect 定位对应 NGAP 消息的 handler

	MyHandlerId.NGAPProcedureType = pdu.Present
	MyHandlerId.NGAPProcedureId = reflect.ValueOf(*pdu).Field(MyHandlerId.NGAPProcedureType).
		Elem().FieldByName("ProcedureCode").FieldByName("Value").Int()

	////fmt.Println("\n\nNGAP MSG ID : ", MyHandlerId.NGAPProcedureType, MyHandlerId.NGAPProcedureId)

	MyHandler, GetMyHandler = HandlerMap[strings.Join(
		[]string{strconv.Itoa(MyHandlerId.NGAPProcedureType),
			strconv.Itoa(int(MyHandlerId.NGAPProcedureId))}, ",")]
	if GetMyHandler {
		MyHandler(ran, pdu)
	} else {
		// 没有实现的 NGAP 消息处理
		//fmt.Println("\n\nnot done, heiheihei\n")
		time.Sleep(time.Second * 5)
	}

	// By uuuuu - end - 2
}

// By uuuuu - end

// 下面是各种 NGAP handler

// By uuuuu - entry
// handle NG setup request 照搬
func HandleNGSetupRequest(ran *context.AmfRan, message *ngapType.NGAPPDU) {
	var globalRANNodeID *ngapType.GlobalRANNodeID
	var rANNodeName *ngapType.RANNodeName
	var supportedTAList *ngapType.SupportedTAList
	var pagingDRX *ngapType.PagingDRX

	var cause ngapType.Cause

	if ran == nil {
		logger.NgapLog.Error("ran is nil")
		return
	}
	if message == nil {
		ran.Log.Error("NGAP Message is nil")
		return
	}
	initiatingMessage := message.InitiatingMessage
	if initiatingMessage == nil {
		ran.Log.Error("Initiating Message is nil")
		return
	}
	nGSetupRequest := initiatingMessage.Value.NGSetupRequest
	if nGSetupRequest == nil {
		ran.Log.Error("NGSetupRequest is nil")
		return
	}
	ran.Log.Info("Handle NG Setup request")
	for i := 0; i < len(nGSetupRequest.ProtocolIEs.List); i++ {
		ie := nGSetupRequest.ProtocolIEs.List[i]
		switch ie.Id.Value {
		case ngapType.ProtocolIEIDGlobalRANNodeID:
			globalRANNodeID = ie.Value.GlobalRANNodeID
			ran.Log.Trace("Decode IE GlobalRANNodeID")
			if globalRANNodeID == nil {
				ran.Log.Error("GlobalRANNodeID is nil")
				return
			}
		case ngapType.ProtocolIEIDSupportedTAList:
			supportedTAList = ie.Value.SupportedTAList
			ran.Log.Trace("Decode IE SupportedTAList")
			if supportedTAList == nil {
				ran.Log.Error("SupportedTAList is nil")
				return
			}
		case ngapType.ProtocolIEIDRANNodeName:
			rANNodeName = ie.Value.RANNodeName
			ran.Log.Trace("Decode IE RANNodeName")
			if rANNodeName == nil {
				ran.Log.Error("RANNodeName is nil")
				return
			}
		case ngapType.ProtocolIEIDDefaultPagingDRX:
			pagingDRX = ie.Value.DefaultPagingDRX
			ran.Log.Trace("Decode IE DefaultPagingDRX")
			if pagingDRX == nil {
				ran.Log.Error("DefaultPagingDRX is nil")
				return
			}
		}
	}

	ran.SetRanId(globalRANNodeID)
	if rANNodeName != nil {
		ran.Name = rANNodeName.Value
	}
	if pagingDRX != nil {
		ran.Log.Tracef("PagingDRX[%d]", pagingDRX.Value)
	}

	for i := 0; i < len(supportedTAList.List); i++ {
		supportedTAItem := supportedTAList.List[i]
		tac := hex.EncodeToString(supportedTAItem.TAC.Value)
		capOfSupportTai := cap(ran.SupportedTAList)
		for j := 0; j < len(supportedTAItem.BroadcastPLMNList.List); j++ {
			supportedTAI := context.NewSupportedTAI()
			supportedTAI.Tai.Tac = tac
			broadcastPLMNItem := supportedTAItem.BroadcastPLMNList.List[j]
			plmnId := ngapConvert.PlmnIdToModels(broadcastPLMNItem.PLMNIdentity)
			supportedTAI.Tai.PlmnId = &plmnId
			capOfSNssaiList := cap(supportedTAI.SNssaiList)
			for k := 0; k < len(broadcastPLMNItem.TAISliceSupportList.List); k++ {
				tAISliceSupportItem := broadcastPLMNItem.TAISliceSupportList.List[k]
				if len(supportedTAI.SNssaiList) < capOfSNssaiList {
					supportedTAI.SNssaiList = append(supportedTAI.SNssaiList, ngapConvert.SNssaiToModels(tAISliceSupportItem.SNSSAI))
				} else {
					break
				}
			}
			ran.Log.Tracef("PLMN_ID[MCC:%s MNC:%s] TAC[%s]", plmnId.Mcc, plmnId.Mnc, tac)
			if len(ran.SupportedTAList) < capOfSupportTai {
				ran.SupportedTAList = append(ran.SupportedTAList, supportedTAI)
			} else {
				break
			}
		}
	}

	if len(ran.SupportedTAList) == 0 {
		ran.Log.Warn("NG-Setup failure: No supported TA exist in NG-Setup request")
		cause.Present = ngapType.CausePresentMisc
		cause.Misc = &ngapType.CauseMisc{
			Value: ngapType.CauseMiscPresentUnspecified,
		}
	} else {
		var found bool
		for i, tai := range ran.SupportedTAList {
			if context.InTaiList(tai.Tai, context.AMF_Self().SupportTaiLists) { // Why here calls a new AMF_Self()? Isn't it nil? util.InitAmfContext
				ran.Log.Tracef("SERVED_TAI_INDEX[%d]", i)
				found = true
				break
			}
		}
		if !found {
			ran.Log.Warn("NG-Setup failure: Cannot find Served TAI in AMF")
			cause.Present = ngapType.CausePresentMisc
			cause.Misc = &ngapType.CauseMisc{
				Value: ngapType.CauseMiscPresentUnknownPLMN,
			}
		}
	}

	if cause.Present == ngapType.CausePresentNothing {
		ngap_message.SendNGSetupResponse(ran)
	} else {
		ngap_message.SendNGSetupFailure(ran, cause)
	}
}

// By uuuuu - end

// By uuuuu - entry
// handle initial UE message 照搬
// 但是 NAS 部分，InitialUEMessage 结束后，
// FSM 跳转到 Authentication，跳到 gmm handler 的 AuthenticationProcedure 有修改
func HandleInitialUEMessage(ran *context.AmfRan, message *ngapType.NGAPPDU) {
	amfSelf := context.AMF_Self()

	var rANUENGAPID *ngapType.RANUENGAPID
	var nASPDU *ngapType.NASPDU
	var userLocationInformation *ngapType.UserLocationInformation
	var rRCEstablishmentCause *ngapType.RRCEstablishmentCause
	var fiveGSTMSI *ngapType.FiveGSTMSI
	// var aMFSetID *ngapType.AMFSetID
	var uEContextRequest *ngapType.UEContextRequest
	// var allowedNSSAI *ngapType.AllowedNSSAI

	var iesCriticalityDiagnostics ngapType.CriticalityDiagnosticsIEList

	if message == nil {
		ran.Log.Error("NGAP Message is nil")
		return
	}

	initiatingMessage := message.InitiatingMessage
	if initiatingMessage == nil {
		ran.Log.Error("Initiating Message is nil")
		return
	}
	initialUEMessage := initiatingMessage.Value.InitialUEMessage
	if initialUEMessage == nil {
		ran.Log.Error("InitialUEMessage is nil")
		return
	}

	ran.Log.Info("Handle Initial UE Message")

	for _, ie := range initialUEMessage.ProtocolIEs.List {
		switch ie.Id.Value {
		case ngapType.ProtocolIEIDRANUENGAPID: // reject
			rANUENGAPID = ie.Value.RANUENGAPID
			ran.Log.Trace("Decode IE RanUeNgapID")
			if rANUENGAPID == nil {
				ran.Log.Error("RanUeNgapID is nil")
				item := buildCriticalityDiagnosticsIEItem(ngapType.CriticalityPresentReject,
					ngapType.ProtocolIEIDRANUENGAPID, ngapType.TypeOfErrorPresentMissing)
				iesCriticalityDiagnostics.List = append(iesCriticalityDiagnostics.List, item)
			}
		case ngapType.ProtocolIEIDNASPDU: // reject
			nASPDU = ie.Value.NASPDU
			ran.Log.Trace("Decode IE NasPdu")
			if nASPDU == nil {
				ran.Log.Error("NasPdu is nil")
				item := buildCriticalityDiagnosticsIEItem(ngapType.CriticalityPresentReject, ngapType.ProtocolIEIDNASPDU,
					ngapType.TypeOfErrorPresentMissing)
				iesCriticalityDiagnostics.List = append(iesCriticalityDiagnostics.List, item)
			}
		case ngapType.ProtocolIEIDUserLocationInformation: // reject
			userLocationInformation = ie.Value.UserLocationInformation
			ran.Log.Trace("Decode IE UserLocationInformation")
			if userLocationInformation == nil {
				ran.Log.Error("UserLocationInformation is nil")
				item := buildCriticalityDiagnosticsIEItem(ngapType.CriticalityPresentReject,
					ngapType.ProtocolIEIDUserLocationInformation, ngapType.TypeOfErrorPresentMissing)
				iesCriticalityDiagnostics.List = append(iesCriticalityDiagnostics.List, item)
			}
		case ngapType.ProtocolIEIDRRCEstablishmentCause: // ignore
			rRCEstablishmentCause = ie.Value.RRCEstablishmentCause
			ran.Log.Trace("Decode IE RRCEstablishmentCause")
		case ngapType.ProtocolIEIDFiveGSTMSI: // optional, reject
			fiveGSTMSI = ie.Value.FiveGSTMSI
			ran.Log.Trace("Decode IE 5G-S-TMSI")
		case ngapType.ProtocolIEIDAMFSetID: // optional, ignore
			// aMFSetID = ie.Value.AMFSetID
			ran.Log.Trace("Decode IE AmfSetID")
		case ngapType.ProtocolIEIDUEContextRequest: // optional, ignore
			uEContextRequest = ie.Value.UEContextRequest
			ran.Log.Trace("Decode IE UEContextRequest")
		case ngapType.ProtocolIEIDAllowedNSSAI: // optional, reject
			// allowedNSSAI = ie.Value.AllowedNSSAI
			ran.Log.Trace("Decode IE Allowed NSSAI")
		}
	}

	if len(iesCriticalityDiagnostics.List) > 0 {
		ran.Log.Trace("Has missing reject IE(s)")

		procedureCode := ngapType.ProcedureCodeInitialUEMessage
		triggeringMessage := ngapType.TriggeringMessagePresentInitiatingMessage
		procedureCriticality := ngapType.CriticalityPresentIgnore
		criticalityDiagnostics := buildCriticalityDiagnostics(&procedureCode, &triggeringMessage, &procedureCriticality,
			&iesCriticalityDiagnostics)
		ngap_message.SendErrorIndication(ran, nil, nil, nil, &criticalityDiagnostics)
	}

	ranUe := ran.RanUeFindByRanUeNgapID(rANUENGAPID.Value)
	if ranUe != nil && ranUe.AmfUe == nil {
		err := ranUe.Remove()
		if err != nil {
			ran.Log.Errorln(err.Error())
		}
		ranUe = nil
	}
	if ranUe == nil {
		var err error
		ranUe, err = ran.NewRanUe(rANUENGAPID.Value)
		if err != nil {
			ran.Log.Errorf("NewRanUe Error: %+v", err)
		}
		ran.Log.Debugf("New RanUe [RanUeNgapID: %d]", ranUe.RanUeNgapId)

		if fiveGSTMSI != nil {
			ranUe.Log.Debug("Receive 5G-S-TMSI")

			servedGuami := amfSelf.ServedGuamiList[0]

			// <5G-S-TMSI> := <AMF Set ID><AMF Pointer><5G-TMSI>
			// GUAMI := <MCC><MNC><AMF Region ID><AMF Set ID><AMF Pointer>
			// 5G-GUTI := <GUAMI><5G-TMSI>
			tmpReginID, _, _ := ngapConvert.AmfIdToNgap(servedGuami.AmfId)
			amfID := ngapConvert.AmfIdToModels(tmpReginID, fiveGSTMSI.AMFSetID.Value, fiveGSTMSI.AMFPointer.Value)

			tmsi := hex.EncodeToString(fiveGSTMSI.FiveGTMSI.Value)

			guti := servedGuami.PlmnId.Mcc + servedGuami.PlmnId.Mnc + amfID + tmsi

			// TODO: invoke Namf_Communication_UEContextTransfer if serving AMF has changed since
			// last Registration Request procedure
			// Described in TS 23.502 4.2.2.2.2 step 4 (without UDSF deployment)

			if amfUe, ok := amfSelf.AmfUeFindByGuti(guti); !ok {
				ranUe.Log.Warnf("Unknown UE [GUTI: %s]", guti)
			} else {
				ranUe.Log.Tracef("find AmfUe [GUTI: %s]", guti)

				if amfUe.CmConnect(ran.AnType) {
					ranUe.Log.Debug("Implicit Deregistration")
					ranUe.Log.Tracef("RanUeNgapID[%d]", amfUe.RanUe[ran.AnType].RanUeNgapId)
					amfUe.DetachRanUe(ran.AnType)
				}
				// TODO: stop Implicit Deregistration timer
				ranUe.Log.Debugf("AmfUe Attach RanUe [RanUeNgapID: %d]", ranUe.RanUeNgapId)
				amfUe.AttachRanUe(ranUe)
			}
		}
	} else {
		ranUe.AmfUe.AttachRanUe(ranUe)
	}

	if userLocationInformation != nil {
		ranUe.UpdateLocation(userLocationInformation)
	}

	if rRCEstablishmentCause != nil {
		ranUe.Log.Tracef("[Initial UE Message] RRC Establishment Cause[%d]", rRCEstablishmentCause.Value)
		ranUe.RRCEstablishmentCause = strconv.Itoa(int(rRCEstablishmentCause.Value))
	}

	if uEContextRequest != nil {
		ran.Log.Debug("Trigger initial Context Setup procedure")
		ranUe.UeContextRequest = true
		// TODO: Trigger Initial Context Setup procedure
	} else {
		ranUe.UeContextRequest = false
	}

	// TS 23.502 4.2.2.2.3 step 6a Nnrf_NFDiscovery_Request (NF type, AMF Set)
	// if aMFSetID != nil {
	// TODO: This is a rerouted message
	// TS 38.413: AMF shall, if supported, use the IE as described in TS 23.502
	// }

	// ng-ran propagate allowedNssai in the rerouted initial ue message (TS 38.413 8.6.5)
	// TS 23.502 4.2.2.2.3 step 4a Nnssf_NSSelection_Get
	// if allowedNSSAI != nil {
	// TODO: AMF should use it as defined in TS 23.502
	// }

	pdu, err := ngap.Encoder(*message)
	if err != nil {
		ran.Log.Errorf("ngap Encoder Error: %+v", err)
	}
	ranUe.InitialUEMessage = pdu
	nas.HandleNAS(ranUe, ngapType.ProcedureCodeInitialUEMessage, nASPDU.Value)
}

// By uuuuu - end

// By uuuuu - entry
// HandleUplinkNasTransport 照搬
// 第一次进入 gmm 的 HandleAuthenticationResponse 函数，内有修改
// 第二次进入 gmm 的 HandleSecurityModeComplete 函数，完全照搬。
//      但还没完，之后跳转到 HandleInitialRegistration 函数，跳到 getSubscribedNssai 函数读取 切片信息，内有修改
//		然而还没完，communicateWithUDM 里 读取了 "subscriptionData.provisionedData.amData" 和 "subscriptionData.provisionedData.smfSelectionSubscriptionData"，内有修改
//		完了还没完，HandleInitialRegistration 之后与 PCF 交流，PCF 从 mongo 读取 "policyData.ues.amData"，但没使用，内有修改
// 第三次进入 gmm 的 HandleRegistrationComplete 函数，完全照搬。这一步在 InitialContextSetupResponse 之后
// 第四次进入 gmm 的 HandleULNASTransport 函数，完全照搬。
//		没完，跳转到 transport5GSMMessage 函数，向 SMF 请求会话建立，并跳转到 pfcp handler - func HandlePfcpSessionEstablishmentResponse，内有修改
func HandleUplinkNasTransport(ran *context.AmfRan, message *ngapType.NGAPPDU) {
	var aMFUENGAPID *ngapType.AMFUENGAPID
	var rANUENGAPID *ngapType.RANUENGAPID
	var nASPDU *ngapType.NASPDU
	var userLocationInformation *ngapType.UserLocationInformation

	if ran == nil {
		logger.NgapLog.Error("ran is nil")
		return
	}
	if message == nil {
		ran.Log.Error("NGAP Message is nil")
		return
	}

	initiatingMessage := message.InitiatingMessage
	if initiatingMessage == nil {
		ran.Log.Error("Initiating Message is nil")
		return
	}

	uplinkNasTransport := initiatingMessage.Value.UplinkNASTransport
	if uplinkNasTransport == nil {
		ran.Log.Error("UplinkNasTransport is nil")
		return
	}
	ran.Log.Info("Handle Uplink Nas Transport")

	for i := 0; i < len(uplinkNasTransport.ProtocolIEs.List); i++ {
		ie := uplinkNasTransport.ProtocolIEs.List[i]
		switch ie.Id.Value {
		case ngapType.ProtocolIEIDAMFUENGAPID:
			aMFUENGAPID = ie.Value.AMFUENGAPID
			ran.Log.Trace("Decode IE AmfUeNgapID")
			if aMFUENGAPID == nil {
				ran.Log.Error("AmfUeNgapID is nil")
				return
			}
		case ngapType.ProtocolIEIDRANUENGAPID:
			rANUENGAPID = ie.Value.RANUENGAPID
			ran.Log.Trace("Decode IE RanUeNgapID")
			if rANUENGAPID == nil {
				ran.Log.Error("RanUeNgapID is nil")
				return
			}
		case ngapType.ProtocolIEIDNASPDU:
			nASPDU = ie.Value.NASPDU
			ran.Log.Trace("Decode IE NasPdu")
			if nASPDU == nil {
				ran.Log.Error("nASPDU is nil")
				return
			}
		case ngapType.ProtocolIEIDUserLocationInformation:
			userLocationInformation = ie.Value.UserLocationInformation
			ran.Log.Trace("Decode IE UserLocationInformation")
			if userLocationInformation == nil {
				ran.Log.Error("UserLocationInformation is nil")
				return
			}
		}
	}

	ranUe := ran.RanUeFindByRanUeNgapID(rANUENGAPID.Value)
	if ranUe == nil {
		ran.Log.Errorf("No UE Context[RanUeNgapID: %d]", rANUENGAPID.Value)
		return
	}
	amfUe := ranUe.AmfUe
	if amfUe == nil {
		err := ranUe.Remove()
		if err != nil {
			ran.Log.Errorf(err.Error())
		}
		ran.Log.Errorf("No UE Context of RanUe with RANUENGAPID[%d] AMFUENGAPID[%d] ",
			rANUENGAPID.Value, aMFUENGAPID.Value)
		return
	}

	ranUe.Log.Infof("Uplink NAS Transport (RAN UE NGAP ID: %d)", ranUe.RanUeNgapId)

	if userLocationInformation != nil {
		ranUe.UpdateLocation(userLocationInformation)
	}

	nas.HandleNAS(ranUe, ngapType.ProcedureCodeUplinkNASTransport, nASPDU.Value)
}

// By uuuuu - end

// By uuuuu - entry
// HandleInitialContextSetupResponse 去掉了 printCriticalityDiagnostics，因为 危险性诊断 正常也不出现，
func HandleInitialContextSetupResponse(ran *context.AmfRan, message *ngapType.NGAPPDU) {
	var aMFUENGAPID *ngapType.AMFUENGAPID
	var rANUENGAPID *ngapType.RANUENGAPID
	var pDUSessionResourceSetupResponseList *ngapType.PDUSessionResourceSetupListCxtRes
	var pDUSessionResourceFailedToSetupList *ngapType.PDUSessionResourceFailedToSetupListCxtRes
	var criticalityDiagnostics *ngapType.CriticalityDiagnostics

	if ran == nil {
		logger.NgapLog.Error("ran is nil")
		return
	}
	if message == nil {
		ran.Log.Error("NGAP Message is nil")
		return
	}
	successfulOutcome := message.SuccessfulOutcome
	if successfulOutcome == nil {
		ran.Log.Error("SuccessfulOutcome is nil")
		return
	}
	initialContextSetupResponse := successfulOutcome.Value.InitialContextSetupResponse
	if initialContextSetupResponse == nil {
		ran.Log.Error("InitialContextSetupResponse is nil")
		return
	}

	ran.Log.Info("Handle Initial Context Setup Response")

	for _, ie := range initialContextSetupResponse.ProtocolIEs.List {
		switch ie.Id.Value {
		case ngapType.ProtocolIEIDAMFUENGAPID:
			aMFUENGAPID = ie.Value.AMFUENGAPID
			ran.Log.Trace("Decode IE AmfUeNgapID")
			if aMFUENGAPID == nil {
				ran.Log.Warn("AmfUeNgapID is nil")
			}
		case ngapType.ProtocolIEIDRANUENGAPID:
			rANUENGAPID = ie.Value.RANUENGAPID
			ran.Log.Trace("Decode IE RanUeNgapID")
			if rANUENGAPID == nil {
				ran.Log.Warn("RanUeNgapID is nil")
			}
		case ngapType.ProtocolIEIDPDUSessionResourceSetupListCxtRes:
			pDUSessionResourceSetupResponseList = ie.Value.PDUSessionResourceSetupListCxtRes
			ran.Log.Trace("Decode IE PDUSessionResourceSetupResponseList")
			if pDUSessionResourceSetupResponseList == nil {
				ran.Log.Warn("PDUSessionResourceSetupResponseList is nil")
			}
		case ngapType.ProtocolIEIDPDUSessionResourceFailedToSetupListCxtRes:
			pDUSessionResourceFailedToSetupList = ie.Value.PDUSessionResourceFailedToSetupListCxtRes
			ran.Log.Trace("Decode IE PDUSessionResourceFailedToSetupList")
			if pDUSessionResourceFailedToSetupList == nil {
				ran.Log.Warn("PDUSessionResourceFailedToSetupList is nil")
			}
		case ngapType.ProtocolIEIDCriticalityDiagnostics:
			criticalityDiagnostics = ie.Value.CriticalityDiagnostics
			ran.Log.Trace("Decode IE Criticality Diagnostics")
			if criticalityDiagnostics == nil {
				ran.Log.Warn("Criticality Diagnostics is nil")
			}
		}
	}

	ranUe := ran.RanUeFindByRanUeNgapID(rANUENGAPID.Value)
	if ranUe == nil {
		ran.Log.Errorf("No UE Context[RanUeNgapID: %d]", rANUENGAPID.Value)
		return
	}
	amfUe := ranUe.AmfUe
	if amfUe == nil {
		ran.Log.Error("amfUe is nil")
		return
	}

	ran.Log.Tracef("RanUeNgapID[%d] AmfUeNgapID[%d]", ranUe.RanUeNgapId, ranUe.AmfUeNgapId)

	if pDUSessionResourceSetupResponseList != nil {
		ranUe.Log.Trace("Send PDUSessionResourceSetupResponseTransfer to SMF")

		for _, item := range pDUSessionResourceSetupResponseList.List {
			pduSessionID := int32(item.PDUSessionID.Value)
			transfer := item.PDUSessionResourceSetupResponseTransfer
			smContext, ok := amfUe.SmContextFindByPDUSessionID(pduSessionID)
			if !ok {
				ranUe.Log.Errorf("SmContext[PDU Session ID:%d] not found", pduSessionID)
			}
			// response, _, _, err := consumer.SendUpdateSmContextN2Info(amfUe, pduSessionID,
			_, _, _, err := consumer.SendUpdateSmContextN2Info(amfUe, smContext,
				models.N2SmInfoType_PDU_RES_SETUP_RSP, transfer)
			if err != nil {
				ranUe.Log.Errorf("SendUpdateSmContextN2Info[PDUSessionResourceSetupResponseTransfer] Error: %+v", err)
			}
			// RAN initiated QoS Flow Mobility in subclause 5.2.2.3.7
			// if response != nil && response.BinaryDataN2SmInformation != nil {
			// TODO: n2SmInfo send to RAN
			// } else if response == nil {
			// TODO: error handling
			// }
		}
	}

	if pDUSessionResourceFailedToSetupList != nil {
		ranUe.Log.Trace("Send PDUSessionResourceSetupUnsuccessfulTransfer to SMF")

		for _, item := range pDUSessionResourceFailedToSetupList.List {
			pduSessionID := int32(item.PDUSessionID.Value)
			transfer := item.PDUSessionResourceSetupUnsuccessfulTransfer
			smContext, ok := amfUe.SmContextFindByPDUSessionID(pduSessionID)
			if !ok {
				ranUe.Log.Errorf("SmContext[PDU Session ID:%d] not found", pduSessionID)
			}
			// response, _, _, err := consumer.SendUpdateSmContextN2Info(amfUe, pduSessionID,
			_, _, _, err := consumer.SendUpdateSmContextN2Info(amfUe, smContext,
				models.N2SmInfoType_PDU_RES_SETUP_FAIL, transfer)
			if err != nil {
				ranUe.Log.Errorf("SendUpdateSmContextN2Info[PDUSessionResourceSetupUnsuccessfulTransfer] Error: %+v", err)
			}

			// if response != nil && response.BinaryDataN2SmInformation != nil {
			// TODO: n2SmInfo send to RAN
			// } else if response == nil {
			// TODO: error handling
			// }
		}
	}

	if ranUe.Ran.AnType == models.AccessType_NON_3_GPP_ACCESS {
		ngap_message.SendDownlinkNasTransport(ranUe, amfUe.RegistrationAcceptForNon3GPPAccess, nil)
	}

	// 正常也不会出现，又嫌搬过来比较麻烦，就注释掉了
	// if criticalityDiagnostics != nil {
	// 	printCriticalityDiagnostics(ran, criticalityDiagnostics)
	// }
}

// By uuuuu - end

// By uuuuu - entry
// HandlePDUSessionResourceSetupResponse 摘自师兄
// 将 SMF 的处理 func smf_context.HandlePDUSessionResourceSetupResponseTransfer 搬来
func HandlePDUSessionResourceSetupResponse(ran *context.AmfRan, message *ngapType.NGAPPDU) {
	var aMFUENGAPID *ngapType.AMFUENGAPID
	var rANUENGAPID *ngapType.RANUENGAPID
	var pDUSessionResourceSetupResponseList *ngapType.PDUSessionResourceSetupListSURes
	var pDUSessionResourceFailedToSetupList *ngapType.PDUSessionResourceFailedToSetupListSURes
	// var criticalityDiagnostics *ngapType.CriticalityDiagnostics

	var ranUe *context.RanUe

	if ran == nil {
		logger.NgapLog.Error("ran is nil")
		return
	}
	if message == nil {
		ran.Log.Error("NGAP Message is nil")
		return
	}
	successfulOutcome := message.SuccessfulOutcome
	if successfulOutcome == nil {
		ran.Log.Error("SuccessfulOutcome is nil")
		return
	}
	pDUSessionResourceSetupResponse := successfulOutcome.Value.PDUSessionResourceSetupResponse
	if pDUSessionResourceSetupResponse == nil {
		ran.Log.Error("PDUSessionResourceSetupResponse is nil")
		return
	}

	ran.Log.Info("Handle PDU Session Resource Setup Response")

	for _, ie := range pDUSessionResourceSetupResponse.ProtocolIEs.List {
		switch ie.Id.Value {
		case ngapType.ProtocolIEIDAMFUENGAPID: // ignore
			aMFUENGAPID = ie.Value.AMFUENGAPID
			ran.Log.Trace("Decode IE AmfUeNgapID")
		case ngapType.ProtocolIEIDRANUENGAPID: // ignore
			rANUENGAPID = ie.Value.RANUENGAPID
			ran.Log.Trace("Decode IE RanUeNgapID")
		case ngapType.ProtocolIEIDPDUSessionResourceSetupListSURes: // ignore
			pDUSessionResourceSetupResponseList = ie.Value.PDUSessionResourceSetupListSURes
			ran.Log.Trace("Decode IE PDUSessionResourceSetupListSURes")
		case ngapType.ProtocolIEIDPDUSessionResourceFailedToSetupListSURes: // ignore
			pDUSessionResourceFailedToSetupList = ie.Value.PDUSessionResourceFailedToSetupListSURes
			ran.Log.Trace("Decode IE PDUSessionResourceFailedToSetupListSURes")
		case ngapType.ProtocolIEIDCriticalityDiagnostics: // optional, ignore
			//criticalityDiagnostics = ie.Value.CriticalityDiagnostics
			ran.Log.Trace("Decode IE CriticalityDiagnostics")
		}
	}

	if rANUENGAPID != nil {
		ranUe = ran.RanUeFindByRanUeNgapID(rANUENGAPID.Value)
		if ranUe == nil {
			ran.Log.Warnf("No UE Context[RanUeNgapID: %d]", rANUENGAPID.Value)
		}
	}

	if aMFUENGAPID != nil {
		ranUe = context.AMF_Self().RanUeFindByAmfUeNgapID(aMFUENGAPID.Value)
		if ranUe == nil {
			ran.Log.Warnf("No UE Context[AmfUeNgapID: %d]", aMFUENGAPID.Value)
			return
		}
	}

	if ranUe != nil {
		ranUe.Log.Tracef("AmfUeNgapID[%d] RanUeNgapID[%d]", ranUe.AmfUeNgapId, ranUe.RanUeNgapId)
		amfUe := ranUe.AmfUe
		if amfUe == nil {
			ranUe.Log.Error("amfUe is nil")
			return
		}

		if pDUSessionResourceSetupResponseList != nil {
			ranUe.Log.Trace("Send PDUSessionResourceSetupResponseTransfer to SMF")

			for _, item := range pDUSessionResourceSetupResponseList.List {
				pduSessionID := int32(item.PDUSessionID.Value)
				transfer := item.PDUSessionResourceSetupResponseTransfer
				smContext, ok := amfUe.SmContextFindByPDUSessionID(pduSessionID)
				if !ok {
					ranUe.Log.Errorf("SmContext[PDU Session ID:%d] not found", pduSessionID)
				}
				// _, _, _, err := consumer.SendUpdateSmContextN2Info(amfUe, smContext,
				// 	models.N2SmInfoType_PDU_RES_SETUP_RSP, transfer)

				smf_logger.PduSessLog.Infoln("In HandlePDUSessionSMContextUpdate")
				smContextRef := smContext.SmContextRef()
				smf_smContext := smf_context.GetSMContext(smContextRef)
				if smf_smContext == nil {
					smf_logger.PduSessLog.Warnln("SMContext is nil, SMContext Ref is not found")
					return
				}

				smf_smContext.SMLock.Lock()
				defer smf_smContext.SMLock.Unlock()

				var sendPFCPModification bool
				tunnel := smf_smContext.Tunnel
				pdrList := []*smf_context.PDR{}
				farList := []*smf_context.FAR{}
				barList := []*smf_context.BAR{}
				qerList := []*smf_context.QER{}

				updateData := models.SmContextUpdateData{}
				updateData.N2SmInfoType = models.N2SmInfoType_PDU_RES_SETUP_RSP
				updateData.N2SmInfo = new(models.RefToBinaryData)
				updateData.N2SmInfo.ContentId = "N2SmInfo"
				updateData.UeLocation = &amfUe.Location

				switch updateData.N2SmInfoType {
				case models.N2SmInfoType_PDU_RES_SETUP_RSP:
					if smf_smContext.SMContextState != smf_context.Active {
						smf_logger.PduSessLog.Warnf("SMContext[%s-%02d] should be Active, but actual %s",
							smf_smContext.Supi, smf_smContext.PDUSessionID, smf_smContext.SMContextState.String())
					}
					smf_smContext.SMContextState = smf_context.ModificationPending
					smf_logger.CtxLog.Traceln("SMContextState Change State: ", smf_smContext.SMContextState.String())
					pdrList = []*smf_context.PDR{}
					farList = []*smf_context.FAR{}

					smf_smContext.PendingUPF = make(smf_context.PendingUPF)
					for _, dataPath := range tunnel.DataPathPool {
						if dataPath.Activated {
							ANUPF := dataPath.FirstDPNode
							DLPDR := ANUPF.DownLinkTunnel.PDR

							DLPDR.FAR.ApplyAction = pfcpType.ApplyAction{Buff: false, Drop: false, Dupl: false, Forw: true, Nocp: false}
							DLPDR.FAR.ForwardingParameters = &smf_context.ForwardingParameters{
								DestinationInterface: pfcpType.DestinationInterface{
									InterfaceValue: pfcpType.DestinationInterfaceAccess,
								},
								NetworkInstance: []byte(smf_smContext.Dnn),
							}
							DLPDR.State = smf_context.RULE_UPDATE
							DLPDR.FAR.State = smf_context.RULE_UPDATE

							pdrList = append(pdrList, DLPDR)
							farList = append(farList, DLPDR.FAR)

							if _, exist := smf_smContext.PendingUPF[ANUPF.GetNodeIP()]; !exist {
								smf_smContext.PendingUPF[ANUPF.GetNodeIP()] = true
							}
						}
					}
					if err := smf_context.
						HandlePDUSessionResourceSetupResponseTransfer(transfer, smf_smContext); err != nil {
						smf_logger.PduSessLog.Errorf("Handle PDUSessionResourceSetupResponseTransfer failed: %+v", err)
					}

					sendPFCPModification = true
					smf_smContext.SMContextState = smf_context.PFCPModification
					smf_logger.CtxLog.Traceln("SMContextState Change State: ", smf_smContext.SMContextState.String())
				}

				switch smf_smContext.SMContextState {
				case smf_context.PFCPModification:
					smf_logger.CtxLog.Traceln("In case PFCPModification")
					if sendPFCPModification {
						defaultPath := smf_smContext.Tunnel.DataPathPool.GetDefaultPath()
						ANUPF := defaultPath.FirstDPNode
						pfcp_message.SendPfcpSessionModificationRequest(ANUPF.UPF.NodeID, smf_smContext, pdrList, farList, barList, qerList)
					}

					PFCPResponseStatus := <-smf_smContext.SBIPFCPCommunicationChan
					switch PFCPResponseStatus {
					case smf_context.SessionUpdateSuccess:
						smf_logger.CtxLog.Traceln("In case SessionUpdateSuccess")
						smf_smContext.SMContextState = smf_context.Active
						smf_logger.CtxLog.Traceln("SMContextState Change State: ", smf_smContext.SMContextState.String())

					case smf_context.SessionUpdateFailed:
						smf_logger.CtxLog.Traceln("In case SessionUpdateFailed")
						smf_smContext.SMContextState = smf_context.Active
						smf_logger.CtxLog.Traceln("SMContextState Change State: ", smf_smContext.SMContextState.String())
					}

				case smf_context.ModificationPending:
					smf_logger.CtxLog.Traceln("In case ModificationPending")
					smf_smContext.SMContextState = smf_context.Active
					smf_logger.CtxLog.Traceln("SMContextState Change State: ", smf_smContext.SMContextState.String())

				default:
					smf_logger.PduSessLog.Warnf("SM Context State [%s] shouldn't be here\n", smf_smContext.SMContextState)
				}
				// if err != nil {
				// 	ranUe.Log.Errorf("SendUpdateSmContextN2Info[PDUSessionResourceSetupResponseTransfer] Error: %+v", err)
				// }

				// RAN initiated QoS Flow Mobility in subclause 5.2.2.3.7
				// if response != nil && response.BinaryDataN2SmInformation != nil {
				// TODO: n2SmInfo send to RAN
				// } else if response == nil {
				// TODO: error handling
				// }
			}
		}

		if pDUSessionResourceFailedToSetupList != nil {
			ranUe.Log.Trace("Send PDUSessionResourceSetupUnsuccessfulTransfer to SMF")

			// for _, item := range pDUSessionResourceFailedToSetupList.List {
			// 	pduSessionID := int32(item.PDUSessionID.Value)
			// 	transfer := item.PDUSessionResourceSetupUnsuccessfulTransfer
			// 	smContext, ok := amfUe.SmContextFindByPDUSessionID(pduSessionID)
			// 	if !ok {
			// 		ranUe.Log.Errorf("SmContext[PDU Session ID:%d] not found", pduSessionID)
			// 	}
			// 	_, _, _, err := consumer.SendUpdateSmContextN2Info(amfUe, smContext,
			// 		models.N2SmInfoType_PDU_RES_SETUP_FAIL, transfer)
			// 	if err != nil {
			// 		ranUe.Log.Errorf("SendUpdateSmContextN2Info[PDUSessionResourceSetupUnsuccessfulTransfer] Error: %+v", err)
			// 	}

			// 	// if response != nil && response.BinaryDataN2SmInformation != nil {
			// 	// TODO: n2SmInfo send to RAN
			// 	// } else if response == nil {
			// 	// TODO: error handling
			// 	// }
			// }
		}
	}

	// if criticalityDiagnostics != nil {
	// 	printCriticalityDiagnostics(ran, criticalityDiagnostics)
	// }
}

// By uuuuu - end

// By uuuuu - entry
// 当 NGAP message 里的一些 ie 为空时，会调用这个函数，建立危险性诊断，类似一种出错记录？
func buildCriticalityDiagnostics(
	procedureCode *int64,
	triggeringMessage *aper.Enumerated,
	procedureCriticality *aper.Enumerated,
	iesCriticalityDiagnostics *ngapType.CriticalityDiagnosticsIEList) (
	criticalityDiagnostics ngapType.CriticalityDiagnostics) {
	if procedureCode != nil {
		criticalityDiagnostics.ProcedureCode = new(ngapType.ProcedureCode)
		criticalityDiagnostics.ProcedureCode.Value = *procedureCode
	}

	if triggeringMessage != nil {
		criticalityDiagnostics.TriggeringMessage = new(ngapType.TriggeringMessage)
		criticalityDiagnostics.TriggeringMessage.Value = *triggeringMessage
	}

	if procedureCriticality != nil {
		criticalityDiagnostics.ProcedureCriticality = new(ngapType.Criticality)
		criticalityDiagnostics.ProcedureCriticality.Value = *procedureCriticality
	}

	if iesCriticalityDiagnostics != nil {
		criticalityDiagnostics.IEsCriticalityDiagnostics = iesCriticalityDiagnostics
	}

	return criticalityDiagnostics
}

// By uuuuu - end

// By uuuuu - entry
// 当 NGAP 里的一些 ie 为空时，会调用这个函数，建立危险性诊断，类似一种出错记录？
func buildCriticalityDiagnosticsIEItem(ieCriticality aper.Enumerated, ieID int64, typeOfErr aper.Enumerated) (
	item ngapType.CriticalityDiagnosticsIEItem) {
	item = ngapType.CriticalityDiagnosticsIEItem{
		IECriticality: ngapType.Criticality{
			Value: ieCriticality,
		},
		IEID: ngapType.ProtocolIEID{
			Value: ieID,
		},
		TypeOfError: ngapType.TypeOfError{
			Value: typeOfErr,
		},
	}

	return item
}

// By uuuuu - end
